#include "Button.h"

module ButtonM {
  provides interface Button;
  uses interface HplMsp430GeneralIO as HplA;
  uses interface HplMsp430GeneralIO as HplB;
  uses interface HplMsp430GeneralIO as HplC;
  uses interface HplMsp430GeneralIO as HplD;
  uses interface HplMsp430GeneralIO as HplE;
  uses interface HplMsp430GeneralIO as HplF;
}

implementation {
  bool pinA;
  bool pinB;
  bool pinC;
  bool pinD;
  bool pinE;
  bool pinF;

  command void Button.start() {
    call HplA.clr();
    call HplA.makeInput();
    call HplB.clr();
    call HplB.makeInput();
    call HplC.clr();
    call HplC.makeInput();
    call HplD.clr();
    call HplD.makeInput();
    call HplE.clr();
    call HplE.makeInput();
    call HplF.clr();
    call HplF.makeInput();
    signal Button.startDone(SUCCESS);
  }

  default event void Button.startDone(error_t error) {
  }
  
  command void Button.stop() {
    signal Button.stopDone(SUCCESS);
  }

  default event void Button.stopDone(error_t error) {
  }

  command void Button.pinvalueA() {
    pinA = call HplA.get();
    if (!pinA) {
      signal Button.pinvalueADone(SUCCESS);
    } else {
      signal Button.pinvalueADone(FAIL);
    }
  }

  default event void Button.pinvalueADone(error_t error) {
  }
  
  command void Button.pinvalueB() {
    pinB = call HplB.get();
    if (!pinB) {
      signal Button.pinvalueBDone(SUCCESS);
    } else {
      signal Button.pinvalueBDone(FAIL);
    }
  }

  default event void Button.pinvalueBDone(error_t error) {
  }

  command void Button.pinvalueC() {
    pinC = call HplC.get();
    if (!pinC) {
      signal Button.pinvalueCDone(SUCCESS);
    } else {
      signal Button.pinvalueCDone(FAIL);
    }
  }

  default event void Button.pinvalueCDone(error_t error) {
  }

  command void Button.pinvalueD() {
    pinD = call HplD.get();
    if (!pinD) {
      signal Button.pinvalueDDone(SUCCESS);
    } else {
      signal Button.pinvalueDDone(FAIL);
    }
  }

  default event void Button.pinvalueDDone(error_t error) {
  }

  command void Button.pinvalueE() {
    pinE = call HplE.get();
    if (!pinE) {
      signal Button.pinvalueEDone(SUCCESS);
    } else {
      signal Button.pinvalueEDone(FAIL);
    }
  }

  default event void Button.pinvalueEDone(error_t error) {
  }

  command void Button.pinvalueF() {
    pinF = call HplF.get();
    if (!pinF) {
      signal Button.pinvalueFDone(SUCCESS);
    } else {
      signal Button.pinvalueFDone(FAIL);
    }
  }
  
  default event void Button.pinvalueFDone(error_t error) {
  }
}
