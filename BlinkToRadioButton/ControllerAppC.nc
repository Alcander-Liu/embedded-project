#include <Timer.h>
#include "BlinkToRadio.h"

configuration ControllerAppC {
}
implementation {
  components MainC;
  components ControllerC as App;
  components new TimerMilliC() as Timer0;
  components ActiveMessageC;
  components new AMSenderC(AM_BLINKTORADIO);
  components ButtonC;
  components JoyStickXC;
  components JoyStickYC;
  components LedsC;

  App.Boot -> MainC;
  App.Timer0 -> Timer0;
  App.Packet -> AMSenderC;
  App.AMPacket -> AMSenderC;
  App.AMControl -> ActiveMessageC;
  App.AMSend -> AMSenderC;
  App.ReadX -> JoyStickXC;
  App.ReadY -> JoyStickYC;
  App.Leds -> LedsC;
  App.Button -> ButtonC;
}
