#include "Button.h"

configuration ButtonC {
  provides interface Button;
}

implementation {
  components ButtonM;
  Button = ButtonM;
  
  components HplMsp430GeneralIOC as Hpl;
  ButtonM.HplA -> Hpl.Port60;
  ButtonM.HplB -> Hpl.Port21;
  ButtonM.HplC -> Hpl.Port61;
  ButtonM.HplD -> Hpl.Port23;
  ButtonM.HplE -> Hpl.Port62;
  ButtonM.HplF -> Hpl.Port26;
}