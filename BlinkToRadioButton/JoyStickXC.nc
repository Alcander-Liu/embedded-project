#include <Msp430Adc12.h>

configuration JoyStickXC {
  provides interface Read<uint16_t>;
}

implementation {
  components JoyStickXP;
  components new AdcReadClientC() as ARC;
  Read = ARC;
  ARC.AdcConfigure -> JoyStickXP;
}