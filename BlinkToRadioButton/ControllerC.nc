#include <Timer.h>
#include "BlinkToRadio.h"
#include "Button.h"

module ControllerC {
  uses interface Boot;
  uses interface Timer<TMilli> as Timer0;
  uses interface Packet;
  uses interface AMPacket;
  uses interface AMSend;
  uses interface SplitControl as AMControl;
  uses interface Button;
  uses interface Read<uint16_t> as ReadX;
  uses interface Read<uint16_t> as ReadY;
  uses interface Leds;
}

implementation {

  uint16_t counter;
  message_t pkt;
  bool busy = FALSE;
  BlinkToRadioMsg* btrpkt;
  uint16_t myCounter = 0;
  uint16_t msgType;
  uint16_t x;
  uint16_t y;

  void setLeds(uint16_t val) {
    if (val & 0x01)
      call Leds.led0On();
    else 
      call Leds.led0Off();
    if (val & 0x02)
      call Leds.led1On();
    else
      call Leds.led1Off();
    if (val & 0x04)
      call Leds.led2On();
    else
      call Leds.led2Off();
  }

  event void Boot.booted() {
    call AMControl.start();
    call Leds.led1On();
    call Leds.led2On();
    call Leds.led0On();
    call Button.start();
  }

  event void AMControl.startDone(error_t err) {
    if (err == SUCCESS) {
      call Timer0.startPeriodic(TIMER_PERIOD_MILLI);
    }
    else {
      call AMControl.start();
    }
  }

  event void Button.startDone(error_t err) {
    if (err == SUCCESS) {
    }
    else {
      call Button.start();
    }
  }

  event void AMControl.stopDone(error_t err) {
  }

  event void Button.stopDone(error_t err) {
  }
 
  event void Timer0.fired() {
    msgType = 0x08;
    
    call ReadX.read();
  }

  void sendMsg() {
    if (!busy) {
      btrpkt = (BlinkToRadioMsg*)(call Packet.getPayload(&pkt, sizeof(BlinkToRadioMsg)));
      if (btrpkt == NULL) {
        return;
      }
      
      btrpkt->nodeid = TOS_NODE_ID;
      btrpkt->counter = msgType;

      if (call AMSend.send(AM_BROADCAST_ADDR, &pkt, sizeof(BlinkToRadioMsg)) == SUCCESS) {
        busy = TRUE;
      }
    }
  }

  event void Button.pinvalueADone(error_t err) {	
    if (err == SUCCESS) {
    	msgType = 0X01;
    }
    call Button.pinvalueB();
  }

  event void Button.pinvalueBDone(error_t err) {
      if (err == SUCCESS) {
	      if (msgType == 0X01) {  // press A and B at the  same time
          msgType = 0X0B;
        } else {
          msgType = 0X09;
        }
      }
      call Button.pinvalueC(); 
  }

  event void Button.pinvalueCDone(error_t err) {
    if (err == SUCCESS) {
    	msgType = 0X02;
    }
    call Button.pinvalueF();  // Skip D and E 'cause they are broken
  }

  event void Button.pinvalueDDone(error_t err) {
    if (err == SUCCESS) {
    }
    call Button.pinvalueE();
  }

  event void Button.pinvalueEDone(error_t err) {
    if (err == SUCCESS) {
    }
    call Button.pinvalueF();
  }

  event void Button.pinvalueFDone(error_t err) {
    if (err == SUCCESS) {
	    if (msgType == 0X02) { // press C and F at the  same time
	      msgType = 0X0C;
	    } else {
        msgType = 0X0A;
      }
    }
    sendMsg();
  }

  void handleXAndY() {
    if ((x>1000&&x<3000)&&(y>1000&&x<3000)) {
      msgType = 0X08;
    }
    if((x+y>4000)&&(y>x)&&(y>3000)) {
	    msgType = 0X04;
    }
    if((x+y<4000)&&(y<x)&&(y<1000)) {
	    msgType = 0X05;
    }
    if((x+y<4000)&&(y>x)&&(x<1000)) {
	    msgType = 0X06;
    }
    if((x+y>4000)&&(y<x)&&(x>1000)) {
	    msgType = 0X07;
    }
    //sendMsg();
    call Button.pinvalueA();
  }

  event void ReadX.readDone(error_t err, uint16_t val) {
      x = val;
      call ReadY.read();
  }

  event void ReadY.readDone(error_t err, uint16_t val) {
      y = val;
      handleXAndY();
  }
  
  event void AMSend.sendDone(message_t* msg, error_t err) {
    if (&pkt == msg) {
      busy = FALSE;
      setLeds(msgType);
    }
  }
}
