#include <Msp430Adc12.h>

configuration JoyStickYC {
  provides interface Read<uint16_t>;
}

implementation {
  components JoyStickYP;
  components new AdcReadClientC() as ARC;
  Read = ARC;
  ARC.AdcConfigure -> JoyStickYP;
}