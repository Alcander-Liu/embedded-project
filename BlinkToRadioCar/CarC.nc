#include <msp430usart.h>
#include "Car.h"

configuration CarC {
  provides interface Car;
}

implementation {
  components CarM;
  Car = CarM;

  components HplMsp430Usart0C;
  components new Msp430Uart0C() as MUart0C;
  CarM.Resource -> MUart0C;
  CarM.HplMsp430Usart -> HplMsp430Usart0C;
}
