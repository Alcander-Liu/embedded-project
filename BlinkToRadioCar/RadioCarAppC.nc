#include <Timer.h>
#include "BlinkToRadio.h"

configuration RadioCarAppC {
}
implementation {
  components MainC;
  components RadioCarC as App;
  components ActiveMessageC;
  components new AMReceiverC(AM_BLINKTORADIO);
  components CarC;
  components LedsC;
  
  // auto move on start
  /*
  components new TimerMilliC() as Timer0;
  components new TimerMilliC() as Timer1;
  components new TimerMilliC() as Timer2;
  components new TimerMilliC() as Timer3;
  */

  App.Car -> CarC;	
  App.Boot -> MainC;
  App.AMControl -> ActiveMessageC;
  App.Receive -> AMReceiverC;
  App.Leds -> LedsC;

  // auto move on start
  /*
  App.Timer0 -> Timer0;
  App.Timer1 -> Timer1;
  App.Timer2 -> Timer2;
  App.Timer3 -> Timer3;
  */
}
