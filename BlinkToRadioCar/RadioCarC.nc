
#include <Timer.h>
#include "BlinkToRadio.h"

module RadioCarC {
  uses interface Boot;
  uses interface Receive;
  uses interface SplitControl as AMControl;
  uses interface Car;
  uses interface Leds;

  // auto move on start
  /*
  uses interface Timer<TMilli> as Timer0;
  uses interface Timer<TMilli> as Timer1;
  uses interface Timer<TMilli> as Timer2;
  uses interface Timer<TMilli> as Timer3;
  */
}

implementation {
  const uint16_t angle_init = 3400;
  uint16_t angle;
  const uint16_t angle_senc_init =3400;
  uint16_t angle_senc;;
  uint16_t counts = 0;
  
  void setLeds(uint16_t val) {
    if (val & 0x01)
      call Leds.led0On();
    else 
      call Leds.led0Off();
    if (val & 0x02)
      call Leds.led1On();
    else
      call Leds.led1Off();
    if (val & 0x04)
      call Leds.led2On();
    else
      call Leds.led2Off();
  }

  event void Car.sendDone() {
  }

  void setCar(uint16_t val) {
    setLeds(val);

    if (val == 0x01){
      angle+=200;
      if(angle>=5000)
        angle = 5000;
      call Car.Angle(angle);
    }

    if (val == 0x02){
      angle_senc+=200;
      if(angle_senc>=5000)
        angle_senc = 5000;
      call Car.Angle_Senc(angle_senc);
    }

    if (val == 0x03)
      call Car.Angle(angle_init);

    if (val == 0x04) {
      //setLeds(0x06);
      call Car.Forward(500);
    }

    if (val == 0x05) {
      //setLeds(0x05);
      call Car.Back(500);
    }

    if (val == 0x06) {
      //setLeds(0x02);
      call Car.Left(500);
    }

    if (val == 0x07) {
      //setLeds(0x01);
      call Car.Right(500);
    }

    if (val == 0x08)
      call Car.Pause();

    if (val == 0x09){
      angle-=200;
      if(angle<=1800)
        angle = 1800;
      call Car.Angle(angle);
    }

    if (val == 0x0A){
      angle_senc-=200;
      if(angle_senc<=1800)
        angle_senc = 1800;
      call Car.Angle_Senc(angle_senc);
    }

    if (val == 0x0C) { 
      call Car.Angle_Senc(angle_senc_init);
    }

    if (val == 0x0B) {
      call Car.Angle(angle_init);
    }
  }
 
  event void Boot.booted() {
    call AMControl.start();
    angle = angle_init;
    angle_senc = angle_senc_init;
    setLeds(0x07);

    // auto move on starte
    /*

    // auto move
    call Car.Forward(500);

    // auto arm move
    call Car.Angle(2000);

    call Timer0.startPeriodic(1000);
    call Timer1.startPeriodic(2000);
    call Timer2.startPeriodic(3000);
    call Timer3.startPeriodic(4000);
    */
  }

  event void Car.readDone(error_t state, uint16_t data) {
    //call Car.Pause();
  }

  event void AMControl.startDone(error_t err) {
    if (err != SUCCESS){
      call AMControl.start();
    }
  }

  event void AMControl.stopDone(error_t err) {
  }

  event message_t* Receive.receive(message_t* msg, void* payload, uint8_t len){
    if (len == sizeof(BlinkToRadioMsg)) {
      BlinkToRadioMsg* btrpkt = (BlinkToRadioMsg*)payload;
      setCar(btrpkt->counter);
    }
    return msg;
  }


  // Auto move on start
  /*
  event void Timer0.fired() {
    // auto move
    call Car.Back();
    
    // auto arm move
    call Car.Angle(5000);
  }

  event void Timer1.fired() {
    // auto move
    call Car.Right();

    // auto arm move
    call Car.Angle_Senc(2000);
  }

  event void Timer2.fired() {
    // auto move
    call Car.Left();

    // auto arm move
    call Car.Angle_Senc(5000);
  }

  event void Timer3.fired() {
    // auto move
    call Car.Pause();

    // auto arm move
    call Car.Angle(angle_init);
    call Car.Angle_Senc(angle_senc_init);
  }
  */

}
