interface Car {
  //command void Start();
  command uint8_t Angle(uint16_t value);
  command uint8_t Angle_Senc(uint16_t value);
  command uint8_t Angle_Third(uint16_t value);
  command uint8_t Forward(uint16_t value);
  command uint8_t Back(uint16_t value);
  command uint8_t Left(uint16_t value);
  command uint8_t Right(uint16_t value);
  command uint8_t Pause();
  event void readDone(error_t state, uint16_t data);
  event void sendDone();
}
