#include <msp430usart.h>

module CarM {
  provides interface Car;
  uses interface Resource;
  uses interface HplMsp430Usart;
}

implementation {

  enum {
      BUF_SIZE = 8,
  };

  uint8_t busy = 0;
  uint8_t buf[BUF_SIZE];
  uint8_t bufPos = 0;
  uint8_t commandType = 0;
  uint8_t dataByteHigh = 0;
  uint8_t dataByteLow = 0;

  msp430_uart_union_config_t  config1 = {
    {
      utxe: 1,
      urxe: 1,
      ubr: UBR_1MHZ_115200,
      umctl: UMCTL_1MHZ_115200,
      ssel: 0x02,
      pena: 0,
      pev: 0,
      spb: 0,
      clen: 1,
      listen: 0,
      mm: 0,
      ckpl: 0,
      urxse: 0,
      urxeie: 0,
      urxwie: 0,
      utxe: 1,
      urxe: 1
    }
  };  

  void sendByte() {
    while(!(call HplMsp430Usart.isTxEmpty()));
    call HplMsp430Usart.tx(buf[bufPos]);
    while(!(call HplMsp430Usart.isTxEmpty()));
    bufPos += 1;
  }

  void sendAll() {
    int i = 0;
    for(i = 0; i < BUF_SIZE; i++) {
      sendByte();
    }
    bufPos = 0;
  }

  void setHeadAndTail() {
    atomic {
      buf[0] = 0x01;
      buf[1] = 0x02;
      buf[5] = 0xFF;
      buf[6] = 0xFF;
      buf[7] = 0x00;
    }
  }
  
  default event void Car.sendDone() {}

  event void Resource.granted() {

    call HplMsp430Usart.setModeUart(&config1);
    call HplMsp430Usart.enableUart();

    atomic U0CTL &= ~SYNC;

    buf[2] = commandType;
    buf[3] = dataByteHigh;
    buf[4] = dataByteLow;
    sendAll();
    
    call Resource.release();
    busy = 0;
    signal Car.sendDone();
  }

  uint8_t wrapAndRequest(uint8_t commandByte, uint8_t dataHigh, uint8_t dataLow) {
    
    if(busy == 1) {
      return EBUSY;
    }

    setHeadAndTail();

    atomic {
      busy = 1;
      commandType = commandByte;
      dataByteHigh = dataHigh;
      dataByteLow = dataLow;
    }

    return call Resource.request();
  }

  command uint8_t Car.Angle(uint16_t value) {
    uint8_t low = value & 0x00FF;
    uint8_t high = value >> 8;
    return wrapAndRequest(0x01, high, low);
  }

  command uint8_t Car.Forward(uint16_t value) {
    uint8_t low = value & 0x00FF;
    uint8_t high = value >> 8;
    return wrapAndRequest(0x02, high, low);
  }

  command uint8_t Car.Back(uint16_t value) {
    uint8_t low = value & 0x00FF;
    uint8_t high = value >> 8;
    return wrapAndRequest(0x03, high, low);
  }

  command uint8_t Car.Left(uint16_t value) {
    uint8_t low = value & 0x00FF;
    uint8_t high = value >> 8;
    return wrapAndRequest(0x04, high, low);
  }

  command uint8_t Car.Right(uint16_t value) {
    uint8_t low = value & 0x00FF;
    uint8_t high = value >> 8;
    return wrapAndRequest(0x05, high, low);
  }

  command uint8_t Car.Pause() {
    return wrapAndRequest(0x06, 0x00, 0x00);
  }

  command uint8_t Car.Angle_Senc(uint16_t value) {
    uint8_t low = value & 0x00FF;
    uint8_t high = value >> 8;
    return wrapAndRequest(0x07, high, low);
  }

  command uint8_t Car.Angle_Third(uint16_t value) {
    uint8_t low = value & 0x00FF;
    uint8_t high = value >> 8;
    return wrapAndRequest(0x08, high, low);
  }  
}
